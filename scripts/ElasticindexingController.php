<?php
namespace app\commands;

use yii\console\Controller;
use yii\elasticsearch\Query;
use app\library\Debug;


class ElasticindexingController extends Controller
{
    public function actionDoindexing($country_code = 'au', $reindex = false)
    {
        /*The algorithm in brief*/
        //if the compnay is not in the processed list
        //get company_name, and web
        //put it in the processed list
        //get table name
        //get all distinct category_l1 from table and company
        //get all distinct category_l2 from table and company
        //get all distinct category_l3 from table and company
        //get all distinct category_l4 from table and company
        //get business description and other info
        //form the data with category JSON
        //insert index
        //update all records to is_indexed=1 where company_name, and web same
        
        Debug::log("Company Indexer processing for " . strtoupper($country_code), 'info');

        $query = new Query;
        $connection = \Yii::$app->getDb();
        $table_name = "amt_au_company";
        $command = $query->createCommand();
        $rs = $command->indexExists($country_code);
        if(!isset($rs) && empty($rs)) {
            $rs = $command->createIndex($country_code);
            if(isset($rs) && !empty($rs)) Debug::log("Index is created", 'debug');
        }
        else {
            Debug::log("Index already exists", 'debug');
        }
        $index = $country_code;
        $type = 'company';

        $className = 'app\models\Amt' . strtoupper($country_code) . 'Company';
        $table_name = 'amt_' . $country_code . '_company';
        $datamodel = new $className();
        $count = $datamodel->find()->count();

        $chunk = ceil($count/10);
        Debug::log("Total data chunk: ".$chunk, 'debug');

        for($i=0;$i<$chunk;$i++) {
            $lower_limit = $i*10;
            $upper_limit = 10;//($i+1)*100;
            Debug::log("Indexing Range: ".$lower_limit." - ".$upper_limit*($i+1), 'debug');
            $companies = "";
            if(!$reindex) {
                $companies = $datamodel->find()
                    ->where('amt_rank != :rank_score and is_indexed = :index_status', ['rank_score'=>0, 'index_status'=>0])
                    ->orderBy('company_key')
                    ->offset($lower_limit)
                    ->limit($upper_limit)->all();
            }
            else {
                $companies = $datamodel->find()
                    ->orderBy('company_key')
                    ->offset($lower_limit)
                    ->limit($upper_limit)->all();
            }

            $size = sizeof($companies);
            Debug::log("Total companies: $size", 'debug');
            if ($size == 0) {
                Debug::log("No companies in this range. Skipping data chunk.", 'debug');
                continue;
            }
            Debug::log("Start Key: ".$companies[0]->company_key, 'debug');
            Debug::log("End Key: ".$companies[sizeof($companies)-1]->company_key, 'debug');
            $company_processed = array();
            foreach($companies as $acompany){
                if($acompany->is_indexed !=1){
                    $company_name = $acompany->company_name;
                    $web = $acompany->web;
                    $company_str = $company_name." ".$web;
                    if(!in_array($company_str, $company_processed)){
                        Debug::log("Indexing :".$company_str, 'debug');
                        array_push($company_processed, $company_str);
                        $business_description = isset($acompany->business_description) ? $acompany->business_description : $acompany->company_description;
                        $amt_rank =$acompany->amt_rank;
                        $company_key = $acompany->company_key;
                        $category1 = [];
                        $category2 = [];
                        $category3 = [];
                        $category4 = [];
                        $sqlcommand = $connection->createCommand("SELECT distinct(category_l1) from $table_name where company_name = '" . mysql_escape_string($company_name) . "' and web = '$web'");
                        $result = $sqlcommand->queryAll();
                        if(!empty($result) && isset($result)){
                            foreach($result as $id=>$row){
                                if(!empty($row) && isset($row))
                                    foreach($row as $id2=>$val){
                                        array_push($category1,$val);
                                    }
                            }
                            if(!empty($category1))
                                Debug::log("Category 1 extraction.....OK", 'debug');
                        }
                        else
                            Debug::log("Category 1 is empty......", 'debug');

                        $sqlcommand = $connection->createCommand("SELECT distinct(category_l2) from $table_name where company_name = '" . mysql_escape_string($company_name) . "' and web = '$web'");
                        $result = $sqlcommand->queryAll();
                        if(!empty($result) && isset($result)){
                            foreach($result as $id=>$row){
                                if(!empty($row) && isset($row))
                                    foreach($row as $id2=>$val){
                                        array_push($category2,$val);
                                    }
                            }
                            if(!empty($category2))
                                Debug::log("Category 2 extraction.....OK", 'debug');
                        }
                        else
                            Debug::log("Category 2 is empty......", 'debug');

                        $sqlcommand = $connection->createCommand("SELECT distinct(category_l3) from $table_name where company_name = '" . mysql_escape_string($company_name) . "' and web = '$web'");
                        $result = $sqlcommand->queryAll();
                        if(!empty($result) && isset($result)){
                            foreach($result as $id=>$row){
                                if(is_array($row) && !empty($row))
                                    foreach($row as $id2=>$val){
                                        array_push($category3,$val);
                                    }
                            }
                            if(!empty($category3))
                                Debug::log("Category 3 extraction.....OK", 'debug');
                        }
                        else
                            Debug::log("Category 3 is empty......", 'debug');

                        $sqlcommand = $connection->createCommand("SELECT distinct(category_l4) from $table_name where company_name = '" . mysql_escape_string($company_name) . "' and web = '$web'");
                        $result = $sqlcommand->queryAll();

                        if(!empty($result) && isset($result)){
                            foreach((array)$result as $id=>$row){
                                if(is_array($row) && !empty($row)){
                                    foreach((array)$row as $id2=>$val){
                                        array_push($category4,$val);
                                    }
                                }
                            }
                            if(!empty($category4))
                                Debug::log("Category 4 extraction.....OK", 'debug');
                        }
                        else
                            Debug::log("Category 4 is empty......", 'debug');

                        //saving the data to the index
                        $data = [
                            "company_key"=> $company_key,
                            "category_l1"=>isset($category1) ? json_encode($category1) : $acompany->category,
                            "category_l2"=>isset($category2) ? json_encode($category2) : $acompany->address,
                            "category_l3"=>isset($category3) ? json_encode($category3) : $acompany->suburb,
                            "category_l4"=>isset($category4) ? json_encode($category4) : $acompany->state,
                            "company_name"=>$company_name,
                            "web" => $web,
                            "business_description"=>isset($business_description) ? $business_description : $acompany->company_description,
                            "amt_rank"=>$amt_rank,
                        ];

                        $command->insert( $index, $type, $data, $id = null, $options = [] );
                        $data = "";
                        $targetcompanies = $datamodel->find()->where(['company_name' => $company_name, 'web'=>$web])->all();
                        Debug::log(sizeof($targetcompanies)." records are set to is_indexed = TRUE", 'debug');
                        foreach ($targetcompanies as $id=>$target){
                            $target->is_indexed = 1;
                            $target->save();
                        }
                        $targetcompanies="";

                    }
                    else {
                        Debug::log("The company is already indexed....OK", 'debug');
                    }
                }
                else {
                    Debug::log("The record is already indexed....OK", 'debug');
                }

            }
        }
        Debug::log("Indexing complete", 'info');
    }
}
