<?php
// Config
error_reporting(E_ALL ^ (E_WARNING | E_NOTICE));
ignore_user_abort(true);
$configPath = dirname(__FILE__) . '/config_rank.ini';

// Load config
if (!file_exists($configPath)) die("Config file not found at $configPath\n");
$config = parse_ini_file($configPath);
if ($config == false) {
    Log::debug("Error: Config file $configPath appears to be invalid.\n", 'error', 'console');
}
ini_set('max_execution_time', $config['max_execution_time']); 

// Validate user input parameters (1)
if (count($argv)!= 4) {
	print "You need to pass 3 parameters\n";
	print "Parameter 1: short form of the country to match table name, e.g. au. This will match amt_au_company table name\n";
	print "Parameter 2: ID of company to start from (i.e. company_key)\n";
	print "Parameter 3: number of data to process from company ID\n";
	print "Usage: php index.php au 1 4999; this ranks the first 5000 companies for AU starting from company with ID 1\n";
	exit;
}
else {
    $country_code = $argv[1];
    if (intval($argv[2]) >= 0 && intval($argv[3]) > 0) {
        $start = $argv[2];
        $count = $argv[3];	
    }
    else {
        print "Invalid number detected, please input integer(s) only\n";
        exit;
    }
}

require(dirname(__FILE__) . '/pagerank.php');
require(dirname(__FILE__) . '/database.php');

try {
    $isSuccess = Database::connect($config['hostname'], $config['username'], $config['password'], $config['database']);
    if ($isSuccess) {
        $end = $start + $count;
        Debug::log("Ranking $country_code with range $start - $end", 'info');
        Debug::log('Successfully connected to database ' . $config['database'], 'debug', 'console');
    }
    else {
        Debug::log('Failed to connect to host ' . $config['hostname'], 'error', 'console');
        exit;
    }
}
catch(Exception $e) {
    Debug::log($e->getMessage(), 'error', 'console');
}

$sql =<<<EOL
    SELECT company_key, company_name, web, amt_rank, rank_json FROM
        amt_{$country_code}_company
    WHERE
        amt_rank = 0 AND
        company_key >= '$start'
    ORDER BY company_key ASC
    LIMIT $count
EOL;

// Retrieve reference to list of companies for the given country (2)
$query = Database::query($sql);
if (!$query) {
    Debug::log("Failed to execute query on table amt_{$country_code}_company", 'error');
    exit;
}

Debug::log("Commence update of social rank score...\n\rProcessing...", 'debug');

// Step through each company in the referenced companies list (3)
while($row = $query->fetch_array()) {
    $companyId = $row['company_key'];
    $url = $row['web'];
    $companyName = $row['company_name'];
    Debug::log("company_key: $companyId", 'debug', 'console');
    Debug::log("compan_name: $companyName", 'debug', 'console'); 
    // If the rank given by amt_rank is 0 or empty skiip to the next company (4)
    if (!empty($row['rank_json']) && intval($row['amt_rank']) != 0) {
        Debug::log('Company already has a rank, rank is: ' . $row['amt_rank'], 'debug');
        $row->free();
        continue;
    }
    $row->free;

    $jsontext = '';
    $ranked_already = 0;
    $rank_text_from_db = '';
    $amt_rank_from_db = '';

    $urlParam = Database::escape($url);

    // If the companies URL is not empty retrieve the first company with that
    // URL that has a rank (not zero or empty) (5)
    if($url != '') {
        $sql =<<<EOL
        SELECT rank_json, amt_rank FROM
            amt_{$country_code}_company
        WHERE
            web = '$urlParam' AND
            amt_rank NOT IN (0, '')
        LIMIT 1
EOL;
        $innerQuery = Database::query($sql);

        // If there is at least 1 company ranked set a flag that the company
        // is already ranked (6)
        if($innerQuery->num_rows > 0) {
            $ranked_already = 1;
            $row = $innerQuery->fetch_row();
            $rank_text_from_db = $row[0];
            $amt_rank_from_db  = $row[1];
            $innerQuery->free();
        }
        else
            $ranked_already = 0;
    }
    // If the company has no URL retrieve the first company with a rank and 
    // that has the current company's name (7)
    else if ($url == '') {
        $nameParam = Database::escape($companyName);
        $sql =<<<EOL
        SELECT rank_json, amt_rank FROM
            amt_{$country_code}_company
        WHERE
            company_name = '$nameParam' AND
            amt_rank NOT IN (0, '')
        LIMIT 1
EOL;
        $innerQuery = Database::query($sql);

        // If there is at least 1 company ranked set a flag that the company
        // is already ranked (7b)
        if ($innerQuery->num_rows > 0) {
            $ranked_already = 1;
            $row = $innerQuery->fetch_row();
            $rank_text_from_db = $row[0];
            $amt_rank_from_db  = $row[1];
            $innerQuery->free();
        }
        else
            $ranked_already = 0;
    }

    // If there is at least one company which set the rank flag then 
    // the company is already ranked (8)
    if ($ranked_already) {
        Debug::log('Company is already ranked... using DB score', 'debug', 'console');
        $jsontext = $rank_text_from_db;
        $amt_rank = $amt_rank_from_db;
    }
    // If the company is not already ranked calculate the companies rank
    // otherwise set it as zero except for LinkedIn count which is calculated
    // based on company name (9)
    else {
        $pr = new PR();
        if ($url != '') {
            Debug::log('Company URL exists so attempt to get hits', 'debug', 'console');
            $active = $pr->getcheckurl($url);
            $Google_rank = $pr->get_google_pagerank($url);
            $Google_hit = $pr->getIndexedPagesGoogle($url);
            $Bing_hit = $pr->getIndexedPagesBing($url);
            $Yahoo_hit = $Bing_hit; //$pr->getIndexedPagesYahoo($url);
            $Alexa_rank = $pr->getAlexaRank1($url);
            $Facebook_hit = $pr->getIndexedPagesFacebook($url);
            $Twitter_hit = $pr->getIndexedPagesTwitter($url);
            $Linkedin_hit = $pr->getLinkedInShares($companyName);
            $Googleplus_hit = $pr->getGooglePlusOnes($url);
        }
        else {
            Debug::log('Company URL does not exist so no hits available', 'debug', 'console');
            $active = 0;
            $Google_rank = 0;
            $Google_hit = 0;
            $Yahoo_hit = 0;
            $Bing_hit = 0;
            $Alexa_rank = 0;
            $Facebook_hit = 0;
            $Twitter_hit = 0;
            $Linkedin_hit = $pr->getLinkedInShares($companyName);
            $Googleplus_hit = 0;
        }
        $jsontext='{"Google_rank":'.$Google_rank.', "Google_hit":'.$Google_hit.', "Yahoo_hit":'.$Yahoo_hit.', "Bing_hit":'.$Bing_hit.', "Alexa_rank":'.$Alexa_rank.', "Facebook_hit":'.$Facebook_hit.', "Twitter_hit":'.$Twitter_hit.', "Linkedin_hit":'.$Linkedin_hit.', "Googleplus_hit":'.$Googleplus_hit.'}';
        if(!empty($jsontext))
            Debug::log('...... successful', 'debug', 'console');
        else
            Debug::log('...... failed to process', 'warn');
        $amt_rank = 0;
        $total_json_key = 9;
        $json_value = array($Google_rank, $Google_hit, $Yahoo_hit, $Bing_hit, $Alexa_rank, $Facebook_hit, $Twitter_hit, $Linkedin_hit, $Googleplus_hit);

        for ($i = 0; $i < $total_json_key; $i++){
            $value = $json_value[$i] == 0 ? 0.1 : $json_value[$i];
            $amt_rank =  $amt_rank + (1/$value);
        }
    }

    // Store the rank for the current company in the database (10)
    $sql =<<<EOL
    UPDATE amt_{$country_code}_company SET
        rank_json='$jsontext',
        is_active='$active',
        amt_rank='$amt_rank'
    WHERE
        company_key='$companyId'
EOL;
    $innerQuery = Database::query($sql);
    if ($innerQuery) Debug::log("Rank ($amt_rank) updated successfully", 'debug');
}

Database::closeConnection();
Debug::log('Update complete', 'debug');
Debug::log('Ranking complete', 'info');

